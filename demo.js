(function($){
	var selected={
		grid: null
	};
	
	function onSelect(e) {
		if (!!selected.grid && selected.grid!=this) {
			selected.grid.clearSelection();
		}
		selected.grid=this;
	}
	
	function onReady() {
		var gridParams={
				dataSource: [{
					id: 1,
					title: "Group 1",
					price: 100,
					weight: 50
				},
				{
					id: 2,
					title: "Group 1",
					price: 100,
					weight: 50
				},
				{
					id: 3,
					title: "Group 1",
					price: 100,
					weight: 50
				},
				{
					id: 4,
					title: "Group 1",
					price: 100,
					weight: 50
				},
				{
					id: 5,
					title: "Group 1",
					price: 100,
					weight: 50
				},
				{
					id: 6,
					title: "Group 1",
					price: 100,
					weight: 50
				},
				{
					id: 7,
					title: "Group 1",
					price: 100,
					weight: 50
				}],
				columns: [
				  {title: "Title", field: "title"},
				  {title: "Price", field: "price", width: "200px"},
				  {title: "Weight", field: "weight", width: "200px"}
				],
				detailTemplate: kendo.template($('#detail').html()),
				detailInit: function (e) {
					var detailTemplate=e.detailRow;
					$('.detail', detailTemplate).kendoGrid(gridParams);
				},
				scrollable: true,
				selectable: true,
				change: onSelect
		};
		
		$('.selector-tree').kendoGrid(gridParams)
	}	
	
	$(document).ready(onReady);
})(jQuery)